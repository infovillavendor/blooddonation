package org.bitbucket.infovillavendor.blooddonor.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.bitbucket.infovillavendor.blooddonor.cells.BloodTypeCell;
import org.bitbucket.infovillavendor.blooddonor.dao.BloodDonorDao;
import org.bitbucket.infovillavendor.blooddonor.dao.BloodTypeDao;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodDonor;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;
import org.bitbucket.infovillavendor.blooddonor.entities.LastDonation;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.DonationType;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.Gender;
import org.bitbucket.infovillavendor.blooddonor.entitywrappers.BloodDonorWrapper;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
@FXMLController(value = "/fxml/blood_donor.fxml", title = "Blood Donor")
public class BloodDonorController {
    @FXML
    @ActionTrigger("deleteDonor")
    Button delete;
    @FXML
    @ActionTrigger("editDonor")
    Button edit;
    @FXML
    @ActionTrigger("editLastDonationDate")
    Button editLastDonationDate;
    @FXML
    @ActionTrigger("loadDonorAddForm")
    Button createNewDonor;

    @FXML
    TableView<BloodDonorWrapper> bloodDonorListTable;
    @FXML
    TableColumn<BloodDonorWrapper, String> donorName;
    @FXML
    TableColumn<BloodDonorWrapper, String> donorMyanmarName;
    @FXML
    TableColumn<BloodDonorWrapper, Integer> donorAge;
    @FXML
    TableColumn<BloodDonorWrapper, Gender> donorGender;
    @FXML
    TableColumn<BloodDonorWrapper, BloodType> donorBloodType;

    @FXML
    TextField name;
    @FXML
    TextField myanmarName;
    @FXML
    TextField age;
    @FXML
    ToggleGroup gender;
    @FXML
    RadioButton male;
    @FXML
    RadioButton female;
    @FXML
    ComboBox<BloodType> bloodTypeComboBox;
    @FXML
    TextField phone;
    @FXML
    TextField email;
    @FXML
    TextField facebook;

    @FXML
    GridPane showGrid;
    @FXML
    GridPane editLastDonationGrid;
    @FXML
    GridPane addGrid;
    @FXML
    GridPane confirmDeleteDonorGrid;
    @FXML
    Text showName;
    @FXML
    Text showMyanmarName;
    @FXML
    Text showAge;
    @FXML
    Text showGender;
    @FXML
    Text showBloodType;
    @FXML
    Text showPhone;
    @FXML
    Text showEmail;
    @FXML
    Text showFacebook;
    @FXML
    Text showLastDonationDate;
    @FXML
    @ActionTrigger("saveDonor")
    Button save;
    @FXML
    @ActionTrigger("clear")
    Button clear;
    @FXML
    @ActionTrigger("addBloodDonor")
    Button create;
    @FXML
    TextField searchName;
    @FXML
    ComboBox<String> searchBloodType;
    @FXML
    ToggleGroup all;
    @FXML
    RadioButton availableDonors;
    @FXML
    RadioButton allDonors;
    @FXML
    ToggleGroup donationType;
    @FXML
    DatePicker lastDonationDate;
    @FXML
    RadioButton internal;
    @FXML
    RadioButton external;
    @FXML
    @ActionTrigger("saveLastDonationDate")
    Button saveLastDonationDate;
    @FXML
    @ActionTrigger("cancelLastDonationDate")
    Button cancelLastDonationDate;
    @FXML
    @ActionTrigger("confirmDeleteDonor")
    Button confirmDeleteDonor;
    @FXML
    @ActionTrigger("cancelDeleteDonor")
    Button cancelDeleteDonor;
    private boolean isMale;
    private BloodDonorWrapper currentBloodDonor;

    @PostConstruct
    public void init() {
        selectMale();
        setupRowClickListener();
        setupTableFocusListener();
        loadTable();
        loadNewDonor();
        setupSearchListeners();
        setupDateConverter();
        selectAll();
    }

    private void selectAll() {
        all.selectToggle(allDonors);
    }

    private void setupDateConverter() {
        String pattern = "dd/M/yyyy";
        lastDonationDate.setPromptText(pattern.toLowerCase());
        lastDonationDate.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateTimeFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);

                } else {
                    return null;
                }
            }
        });
    }

    private void setupSearchListeners() {
        searchName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String bloodTypeInSearch = searchBloodType.getValue();
                if (bloodTypeInSearch.equals("All"))
                    bloodTypeInSearch = null;
                loadTableBySearchTerms(newValue, bloodTypeInSearch, all.getSelectedToggle() == availableDonors);
            }
        });

        searchBloodType.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String bloodTypeInSearch = newValue;
                if (bloodTypeInSearch.equals("All"))
                    bloodTypeInSearch = null;
                loadTableBySearchTerms(searchName.getText(), bloodTypeInSearch, all.getSelectedToggle() == availableDonors);
            }
        });

        all.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                String bloodTypeInSearch = searchBloodType.getValue();
                if (bloodTypeInSearch.equals("All"))
                    bloodTypeInSearch = null;
                loadTableBySearchTerms(searchName.getText(), bloodTypeInSearch, all.getSelectedToggle() == availableDonors);
            }
        });
    }

    private void setupRowClickListener() {
        bloodDonorListTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BloodDonorWrapper>() {
            @Override
            public void changed(ObservableValue<? extends BloodDonorWrapper> observable, BloodDonorWrapper oldValue, BloodDonorWrapper newValue) {
                currentBloodDonor = newValue;
                if (currentBloodDonor == null)
                    loadDonorAddForm();
                else
                    loadDonorDetails();
            }
        });
    }

    private void setupTableFocusListener() {
        bloodDonorListTable.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue && bloodDonorListTable.getSelectionModel().getSelectedItem() == currentBloodDonor) {
                    loadDonorDetails();
                }
            }
        });

    }

    private void loadDonorDetails() {
        showGrid.setVisible(true);
        addGrid.setVisible(false);
        editLastDonationGrid.setVisible(false);
        confirmDeleteDonorGrid.setVisible(false);
        showGrid.toFront();
        if (currentBloodDonor != null) {
            showName.setText(currentBloodDonor.getName());
            showMyanmarName.setText(currentBloodDonor.getMyanmarName());
            showAge.setText(currentBloodDonor.getAge() + "");
            showGender.setText(currentBloodDonor.getGender());
            showBloodType.setText(currentBloodDonor.getBloodType());
            showPhone.setText(currentBloodDonor.getPhone());
            showEmail.setText(currentBloodDonor.getEmail());
            showFacebook.setText(currentBloodDonor.getFacebook());
            String lastDonationDateString = "-";
            if (currentBloodDonor.getBloodDonor().getLastDonation() != null) {
                Date lastDonationDate = currentBloodDonor.getBloodDonor().getLastDonation().getLastDonationDate();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
                lastDonationDateString = simpleDateFormat.format(lastDonationDate);
            }
            showLastDonationDate.setText(lastDonationDateString);
        }
    }

    private void loadTable() {

        List<BloodDonor> bloodDonors = BloodDonorDao.getBloodDonors();
        List<BloodDonorWrapper> bloodDonorWrappers = new ArrayList<BloodDonorWrapper>();
        for (BloodDonor bloodDonor : bloodDonors) {
            bloodDonorWrappers.add(new BloodDonorWrapper(bloodDonor));
        }
        bloodDonorListTable.getItems().setAll(bloodDonorWrappers);
    }

    private void loadTableBySearchTerms(String name, String bloodType, boolean available) {

        List<BloodDonor> bloodDonors = null;
        if (!available)
            bloodDonors = BloodDonorDao.findBloodDonorsByNameLikeAndBloodType(name, bloodType);
        else
            bloodDonors = BloodDonorDao.findAvailableBloodDonorsByNameLikeAndBloodType(name, bloodType);
        List<BloodDonorWrapper> bloodDonorWrappers = new ArrayList<>();
        for (BloodDonor bloodDonor : bloodDonors) {
            bloodDonorWrappers.add(new BloodDonorWrapper(bloodDonor));
        }

        BloodDonorWrapper previousBloodDonorWrapper = currentBloodDonor;
        bloodDonorListTable.getItems().setAll(bloodDonorWrappers);
        if (bloodDonorWrappers.contains(previousBloodDonorWrapper)) {
            BloodDonorWrapper bloodDonorWrapperToSelect = bloodDonorWrappers.get(bloodDonorWrappers.indexOf(previousBloodDonorWrapper));
            bloodDonorListTable.getSelectionModel().select(bloodDonorWrapperToSelect);
        }
    }

    private void clearFields() {
        name.clear();
        myanmarName.clear();
        age.clear();
        phone.clear();
        email.clear();
        facebook.clear();
        bloodTypeComboBox.getSelectionModel().clearSelection();
        bloodTypeComboBox.setValue(bloodTypeComboBox.getItems().get(0));
        selectMale();
    }

    private void selectMale() {
        gender.selectToggle(male);
        isMale = true;
    }

    private void loadNewDonor() {
        List<BloodType> bloodTypes = BloodTypeDao.getBloodTypes();
        bloodTypeComboBox.getItems().setAll(bloodTypes);
        bloodTypeComboBox.setValue(bloodTypeComboBox.getItems().get(0));

        save.setVisible(false);
        create.setVisible(true);
        create.toFront();

        bloodTypeComboBox.setButtonCell(new BloodTypeCell());
        bloodTypeComboBox.setCellFactory(new Callback<ListView<BloodType>, ListCell<BloodType>>() {
                                             @Override
                                             public ListCell<BloodType> call(ListView<BloodType> param) {
                                                 return new BloodTypeCell();
                                             }
                                         }

        );

        gender.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (gender.getSelectedToggle() == male)
                    isMale = true;
                else
                    isMale = false;
            }
        });
    }

    private void refreshTable() {
        bloodDonorListTable.getItems().clear();
        loadTable();
    }

    private void loadDonorForm() {
        showGrid.setVisible(false);
        editLastDonationGrid.setVisible(false);
        confirmDeleteDonorGrid.setVisible(false);
        addGrid.setVisible(true);
        addGrid.toFront();
        clearFields();
    }

    private void clearLastDonationFields() {
        lastDonationDate.setValue(null);
        donationType.selectToggle(internal);

    }

    @ActionMethod("addBloodDonorMethod")
    public void addBloodDonorMethod() {
        BloodDonor bloodDonor = new BloodDonor();
        bloodDonor.setName(name.getText());
        bloodDonor.setMyanmarName(myanmarName.getText());
        bloodDonor.setBloodType(bloodTypeComboBox.getValue());
        bloodDonor.setAge(Integer.parseInt(age.getText()));
        bloodDonor.setGender(isMale ? Gender.MALE : Gender.FEMALE);
        bloodDonor.setPhone(phone.getText());
        bloodDonor.setEmail(email.getText());
        bloodDonor.setFacebook(facebook.getText());
        bloodDonor.setCreateDate(new Date());
        BloodDonorDao.saveBloodDonor(bloodDonor);
        refreshTable();
        clearFields();
    }

    @ActionMethod("saveDonor")
    public void saveDonor() {
        BloodDonor bloodDonor = currentBloodDonor.getBloodDonor();
        bloodDonor.setName(name.getText());
        bloodDonor.setMyanmarName(myanmarName.getText());
        bloodDonor.setBloodType(bloodTypeComboBox.getValue());
        bloodDonor.setAge(Integer.parseInt(age.getText()));
        bloodDonor.setGender(isMale ? Gender.MALE : Gender.FEMALE);
        bloodDonor.setPhone(phone.getText());
        bloodDonor.setEmail(email.getText());
        bloodDonor.setFacebook(facebook.getText());
        bloodDonor.setCreateDate(new Date());
        BloodDonorDao.updateBloodDonor(bloodDonor);
        refreshTable();
        clearFields();
    }

    @ActionMethod("clear")
    public void clear() {
        clearFields();
    }

    @ActionMethod("loadDonorAddForm")
    public void loadDonorAddForm() {
        save.setVisible(false);
        create.setVisible(true);
        create.toFront();
        loadDonorForm();
    }

    @ActionMethod("confirmDeleteDonor")
    public void confirmDeleteDonor() {
        BloodDonorDao.deleteBloodDonor(currentBloodDonor.getBloodDonor());
        currentBloodDonor = null;
        refreshTable();

    }

    @ActionMethod("cancelDeleteDonor")
    public void cancelDeleteDonor() {
        loadDonorDetails();
    }

    @ActionMethod("deleteDonor")
    public void deleteDonor() {
        showGrid.setVisible(false);
        addGrid.setVisible(false);
        editLastDonationGrid.setVisible(false);
        confirmDeleteDonorGrid.setVisible(true);

    }

    @ActionMethod("editDonor")
    public void editDonor() {
        loadDonorForm();
        create.setVisible(false);
        save.setVisible(true);
        save.toFront();

        if (currentBloodDonor != null) {
            name.setText(currentBloodDonor.getBloodDonor().getName());
            myanmarName.setText(currentBloodDonor.getBloodDonor().getMyanmarName());
            age.setText(currentBloodDonor.getAge());
            gender.selectToggle(currentBloodDonor.getBloodDonor().getGender() == Gender.MALE ? male : female);
            List<BloodType> bloodTypesInSelection = bloodTypeComboBox.getItems();
            BloodType selectedBloodType = null;
            for (BloodType bloodType : bloodTypesInSelection) {
                if (bloodType.getId() == currentBloodDonor.getBloodDonor().getBloodType().getId())
                    selectedBloodType = bloodType;
            }
            bloodTypeComboBox.setValue(selectedBloodType);
            phone.setText(currentBloodDonor.getBloodDonor().getPhone());
            email.setText(currentBloodDonor.getBloodDonor().getEmail());
            facebook.setText(currentBloodDonor.getBloodDonor().getFacebook());

        }
    }

    @ActionMethod("editLastDonationDate")
    public void editLastDonationDate() {
        showGrid.setVisible(false);
        addGrid.setVisible(false);
        confirmDeleteDonorGrid.setVisible(false);
        editLastDonationGrid.setVisible(true);
        editLastDonationGrid.toFront();
        LastDonation lastDonation = currentBloodDonor.getBloodDonor().getLastDonation();
        if (lastDonation == null) {
            donationType.selectToggle(internal);

        } else {
            Date utilDate = lastDonation.getLastDonationDate();
            Instant instant = Instant.ofEpochMilli(utilDate.getTime());
            LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
            lastDonationDate.setValue(localDate);
            if (lastDonation.getDonationType() == DonationType.INTERNAL)
                donationType.selectToggle(internal);
            else
                donationType.selectToggle(external);
        }
    }

    @ActionMethod("saveLastDonationDate")
    public void saveLastDonationDate() {
        LastDonation lastDonation = currentBloodDonor.getBloodDonor().getLastDonation();
        boolean isNewLastDonation = false;
        if (lastDonation == null) {
            lastDonation = new LastDonation();
            currentBloodDonor.getBloodDonor().setLastDonation(lastDonation);
            isNewLastDonation = true;

        }

        lastDonation.setDonationType(donationType.getSelectedToggle() == internal ? DonationType.INTERNAL : DonationType.EXTERNAL);
        LocalDate selectedLastDonationDate = lastDonationDate.getValue();
        Date utilDate = Date.from(selectedLastDonationDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        lastDonation.setLastDonationDate(utilDate);
        lastDonation.setBloodDonor(currentBloodDonor.getBloodDonor());
        BloodDonorDao.updateBloodDonor(currentBloodDonor.getBloodDonor());
        clearLastDonationFields();
        loadDonorDetails();

        BloodDonorWrapper previousBloodDonorWrapper = currentBloodDonor;
        String bloodTypeInSearch = searchBloodType.getValue();
        if (bloodTypeInSearch.equals("All"))
            bloodTypeInSearch = null;
        loadTableBySearchTerms(searchName.getText(), bloodTypeInSearch, all.getSelectedToggle() == availableDonors);
        List<BloodDonorWrapper> bloodDonorWrappers = bloodDonorListTable.getItems();
        if (bloodDonorWrappers.contains(previousBloodDonorWrapper)) {
            BloodDonorWrapper bloodDonorWrapperToSelect = bloodDonorWrappers.get(bloodDonorWrappers.indexOf(previousBloodDonorWrapper));
            bloodDonorListTable.getSelectionModel().select(bloodDonorWrapperToSelect);
        }
    }

    @ActionMethod("cancelLastDonationDate")
    public void cancelLastDonationDate() {
        clearLastDonationFields();
        loadDonorDetails();

    }

}
