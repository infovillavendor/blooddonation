package org.bitbucket.infovillavendor.blooddonor.dao;

import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
public class BloodTypeDao extends Dao {
    public static List<BloodType> getBloodTypes() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<BloodType> query = em.createNamedQuery("BloodType.findAll", BloodType.class);
        List<BloodType> bloodTypes = query.getResultList();
        return bloodTypes;
    }
}
