package org.bitbucket.infovillavendor.blooddonor.dao.converters;

import org.bitbucket.infovillavendor.blooddonor.entities.enums.DonationType;

import javax.persistence.AttributeConverter;

/**
 * Created by Thant Zin Oo on 10/27/2014.
 */
public class DonationTypeConverter implements AttributeConverter<DonationType, String> {

    @Override
    public String convertToDatabaseColumn(DonationType donationType) {
        if (donationType == DonationType.INTERNAL)
            return "I";
        else
            return "E";
    }

    @Override
    public DonationType convertToEntityAttribute(String genderStr) {
        switch (genderStr) {
            case "I":
                return DonationType.INTERNAL;
            default:
                return DonationType.EXTERNAL;
        }
    }
}
