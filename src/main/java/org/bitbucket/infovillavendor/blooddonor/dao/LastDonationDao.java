package org.bitbucket.infovillavendor.blooddonor.dao;

import org.bitbucket.infovillavendor.blooddonor.entities.LastDonation;

import javax.persistence.EntityManager;

/**
 * Created by Thant Zin Oo on 10/28/2014.
 */
public class LastDonationDao extends Dao {
    public static void saveLastDonation(LastDonation lastDonation) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(lastDonation);
        em.getTransaction().commit();
        em.close();
    }
}
