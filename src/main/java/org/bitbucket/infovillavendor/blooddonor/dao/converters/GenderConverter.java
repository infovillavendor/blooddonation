package org.bitbucket.infovillavendor.blooddonor.dao.converters;

import org.bitbucket.infovillavendor.blooddonor.entities.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Thant Zin Oo on 10/21/2014.
 */
@Converter
public class GenderConverter implements AttributeConverter<Gender, String> {
    @Override
    public String convertToDatabaseColumn(Gender gender) {
        if (gender == Gender.MALE)
            return "M";
        else
            return "F";
    }

    @Override
    public Gender convertToEntityAttribute(String genderStr) {
        switch (genderStr) {
            case "M":
                return Gender.MALE;
            default:
                return Gender.FEMALE;
        }

    }
}
