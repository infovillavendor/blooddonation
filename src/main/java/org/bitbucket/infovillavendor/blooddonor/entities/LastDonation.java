package org.bitbucket.infovillavendor.blooddonor.entities;

import org.bitbucket.infovillavendor.blooddonor.dao.converters.DonationTypeConverter;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.DonationType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thant Zin Oo on 10/27/2014.
 */
@Entity
@Table(name = "last_donation")
public class LastDonation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "last_donation_date")
    @Temporal(value = TemporalType.DATE)
    private Date lastDonationDate;
    @Column(name = "donation_type")
    @Convert(converter = DonationTypeConverter.class)
    private DonationType donationType;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "donor_id")
    private BloodDonor bloodDonor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getLastDonationDate() {
        return lastDonationDate;
    }

    public void setLastDonationDate(Date lastDonationDate) {
        this.lastDonationDate = lastDonationDate;
    }

    public DonationType getDonationType() {
        return donationType;
    }

    public void setDonationType(DonationType donationType) {
        this.donationType = donationType;
    }

    public BloodDonor getBloodDonor() {
        return bloodDonor;
    }

    public void setBloodDonor(BloodDonor bloodDonor) {
        this.bloodDonor = bloodDonor;
    }
}
