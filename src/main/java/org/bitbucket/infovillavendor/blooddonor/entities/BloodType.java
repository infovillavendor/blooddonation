package org.bitbucket.infovillavendor.blooddonor.entities;

import javax.persistence.*;

/**
 * Created by Sandah Aung on 15/10/14.
 */

@NamedQueries({
        @NamedQuery(name = "BloodType.findAll",
                query = "SELECT e FROM BloodType e"),
        @NamedQuery(name = "BloodType.findByName",
                query = "SELECT e FROM BloodType e WHERE e.bloodType = :bloodType"),
})

@Entity
@Table(name = "blood_type")
public class BloodType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "blood_type")
    private String bloodType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }
}
