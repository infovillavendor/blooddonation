package org.bitbucket.infovillavendor.blooddonor;

import javafx.application.Application;
import javafx.stage.Stage;
import org.bitbucket.infovillavendor.blooddonor.controllers.BloodDonorController;
import org.bitbucket.infovillavendor.blooddonor.dao.Dao;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.action.FlowActionChain;
import org.datafx.controller.flow.action.FlowMethodAction;

public class Launcher extends Application {
    public static void main(String[] args) {
        System.out.println("");
        /*EntityManagerFactory emf = Persistence.createEntityManagerFactory("BloodDonorPU");
        EntityManager em = emf.createEntityManager();
        TypedQuery<BloodType> bloodTypeTypedQuery = em.createNamedQuery("BloodType.findAll", BloodType.class);
        List<BloodType> bloodTypeList = bloodTypeTypedQuery.getResultList();

        for (BloodType bloodType : bloodTypeList) {
            System.out.println("blood type: " + bloodType.getBloodType());
        }*/

        /*EntityManagerFactory emf = Persistence.createEntityManagerFactory("BloodDonorPU");
        EntityManager em = emf.createEntityManager();
        TypedQuery<BloodDonor> bloodDonorTypedQuery = em.createNamedQuery("BloodDonor.findAll", BloodDonor.class);
        List<BloodDonor> bloodDonorList = bloodDonorTypedQuery.getResultList();

        for (BloodDonor bloodDonor : bloodDonorList) {
            System.out.println("blood type: " + bloodDonor.getName());
        }*/

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new Flow(BloodDonorController.class)
                .withAction(BloodDonorController.class, "addBloodDonor", new FlowActionChain(new FlowMethodAction("addBloodDonorMethod")))
                .startInStage(primaryStage);

    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Dao.emf.close();
    }
}
