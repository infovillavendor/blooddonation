package org.bitbucket.infovillavendor.blooddonor;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by Sandah Aung on 6/11/14.
 */
public class TextConverter {
    private static ScriptEngine jsScriptEngine;
    private static Reader jsFileReader;

    static {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        jsScriptEngine = scriptEngineManager.getEngineByExtension("js");
        jsFileReader = new InputStreamReader(TextConverter.class.getResourceAsStream("/js/tlconvert.js"));
    }

    public static String toUnicode(String input) {
        jsScriptEngine.put("inputString", input);
        try {
            jsScriptEngine.eval(jsFileReader);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return (String) jsScriptEngine.get("outputString");
    }
}
